describe('personService', function() {
	// the doc says 'module(...)' can be called directly, but it didn't work for
	// me, hence the angular.mock.module(...)
	beforeEach(angular.mock.module('person'));

	var personService;

	// Here we inject the Angular injectable 'personService'
	// Read more about underscore wrapping here:
	// https://docs.angularjs.org/api/ngMock/function/angular.mock.inject
	beforeEach(inject(function(_personService_) {
		personService = _personService_;
	}));

	describe('the constructor function \'new\'', function() {
		it('should create and return a person object without an address', function() {
			var firstname = "foo";
			var lastname = "bar";
			var person = personService.new(firstname, lastname);

			expect(person.firstname).toEqual(firstname);
			expect(person.lastname).toEqual(lastname);
			expect(person.address).not.toBeDefined();
		});
		it('should not create the same person twice', function() {
			var firstname = "foo";
			var lastname = "bar";
			personService.new(firstname, lastname);

			expect(personService.getAll().length).toBe(1);
			personService.new(firstname, lastname);
		});
	});
});
