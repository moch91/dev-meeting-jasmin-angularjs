var module = angular.module('person', []);

module.service('personService', function() {
	/**
	 * Returns the user information in human-readable form
	 */
	var toString = function() {
		return (this.address ? this.address + ' ': '') + this.firstname + " " + this.lastname;
	};

	var equals = function(person) {
		return false;
	};

	var persons = [];
	return {
		/**
		 * Create and return a new person. If the person already exists, just
		 * return it.
		 */
		new: function(firstname, lastname, address) {
			if (typeof firstname !== 'string' || firstname.trim().length === 0)
				throw new Error('Argument "firstname" must be a non-empty string! Value: "' + firstname + '"');

			if (typeof lastname !== 'string' || lastname.trim().length === 0)
				throw new Error('Argument "lastname" must be a non-empty string! Value: "' + lastname + '"');

			var out = {
				firstname: firstname.trim(),
				lastname: lastname.trim(),
				toString: toString
			};

			if (typeof address === "string" && address.trim().length > 0)
				out.address = address.trim();

			persons.push(out);
			return out;

		},

		getAll: function() {
			return persons;
		},
		get: function(firstnameOrPerson, lastname) {
			var firstname = typeof firstnameOrPerson === "string" ?
				firstnameOrPerson.toLowerCase() :
				firstnameOrPerson.firstnameOrPerson.toLowerCase();
			lastname = typeof lastname === "string" ?
				lastname.toLowerCase() :
				undefined;

			for (var i = 0; i < persons.length; i++) {
				var p = persons[i];
				if (p.firstname.toLowerCase() === firstname && p.lastname.toLowerCase() === lastname)
					return p;
			}

			return null;
		},

		/**
		 * Filters the persons list by the passed criterion.
		 *
		 * @string criterion is a string that matches either first or lastname
		 *
		 * return Array containing all persons whose first or lastname match the
		 * criterion. If criterion is not a string or empty, all persons are
		 * returned.
		 */
		filter: function(criterion) {
			if (typeof criterion !== "string" || criterion.trim().length === 0)
				return persons;

			criterion = criterion.toLowerCase();

			var out = [];
			for (var i = 0; i < persons.length; i++) {
				var p = persons[i];
				if (p.firstname.toLowerCase().match(criterion) || p.lastname.toLowerCase().match(criterion))
					out.push(p);
			}

			return out;
		},
		isPerson: function(object) {
			if (typeof object !== 'object' || object === undefined || object === null)
				return false;

			return object.hasOwnProperty('firstname') && object.hasOwnProperty('lastname');
		},
		remove: function(firstnameOrPerson, lastname) {
			var p = this.get(firstnameOrPerson, lastname);

			if (p) {
				for (var i = 0; i < persons.length; i++) {
					if (persons[i].equals(p)) {
						persons.splice(i);
						return;
					}
				}
			}
		},
		clear: function() {
			persons = [];
		}
	};
});
