var module = angular.module('app', ['person']);

module.controller('PersonCtrl', function($scope, personService) {
	$scope.persons = personService;

	// init
	personService.new('Mega', 'Mohi');
	personService.new('Urs', 'Unglaublich', 'Herr');
	personService.new('Tom', 'Turbo', 'Herr');
	personService.new('Oskar', 'vom Waldemmengrund', 'Don');

})
.controller('PersonFormCtrl', function($scope, personService) {
	var reset = function() {
		$scope.firstname = '';
		$scope.lastname = '';
	};

	$scope.newPerson = function(firstname, lastname) {
		try {
			var p = personService.new(firstname, lastname);
			reset();
		} catch (e) {
			console.error(e);
			alert('Please enter first and lastname');
		}
	};
});
